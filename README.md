Code challenge for WorldPay
========================

Build and run using SBT.  It will start a server on port 9090.

You can then post adverts to

    localhost:9090/addAdvert.  
    
A successful POST will return an id which you can use to retrieve the advert from:

    localhost:9090/advert/{id}


