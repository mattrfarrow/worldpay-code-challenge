package com.example;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

class AdvertService {

    private final Map<String, Advert> adverts = new HashMap<>();

    String addAdvert(Advert advert) {
        synchronized (adverts) {
            String key = "advert-"+adverts.size();
            adverts.put(key, advert);
            return key;
        }
    }

    Optional<Advert> getAdvert(String key) {
        synchronized (adverts) {
            return Optional.ofNullable(adverts.get(key));
        }
    }

}
