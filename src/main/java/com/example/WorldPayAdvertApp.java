package com.example;

import akka.NotUsed;
import akka.actor.ActorSystem;
import akka.http.javadsl.ConnectHttp;
import akka.http.javadsl.Http;
import akka.http.javadsl.ServerBinding;
import akka.http.javadsl.marshallers.jackson.Jackson;

import akka.http.javadsl.model.*;
import akka.http.javadsl.server.AllDirectives;
import akka.http.javadsl.server.Route;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Flow;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

public class WorldPayAdvertApp extends AllDirectives {

    private AdvertService advertService = new AdvertService();
    private static final int PORT = 9090;

    public static void main(String[] args) throws IOException {
        ActorSystem system = ActorSystem.create();
        final Http http = Http.get(system);
        final ActorMaterializer materializer = ActorMaterializer.create(system);

        final WorldPayAdvertApp app = new WorldPayAdvertApp();
        final Flow<HttpRequest, HttpResponse, NotUsed> routeFlow = app.createRoute().flow(system, materializer);
        final CompletionStage<ServerBinding> binding = http.bindAndHandle(routeFlow, ConnectHttp.toHost("localhost", PORT), materializer);

        System.out.println("App running on PORT "+ PORT +".  Press ENTER to exit");
        //noinspection ResultOfMethodCallIgnored
        System.in.read();

        binding
                .thenCompose(ServerBinding::unbind)
                .thenAccept(unbound -> system.terminate());
    }

    Route createRoute() {
        Route addAdvertRoute =
                entity(Jackson.unmarshaller(Advert.class), advert -> {
                    String createdId = advertService.addAdvert(advert);
                    return complete(HttpEntities.create(ContentTypes.APPLICATION_JSON, "{ \"id\": \""+createdId+"\"}"));
                }
               );

        Route getAdvertRoute =
                parameterOptional("id", optId -> {
                    if(optId.isPresent()) {
                        Optional<Advert> advertOpt = advertService.getAdvert(optId.get());
                        if(advertOpt.isPresent()) {
                            Advert advert = advertOpt.get();
                            return complete(StatusCodes.OK, advert, Jackson.marshaller());
                        } else {
                            return complete(StatusCodes.NOT_FOUND, "No such advert");
                        }
                    } else {
                        return complete(StatusCodes.BAD_REQUEST, "No id parameter");
                    }
                });

        return route(
                post(() ->
                        path("addadvert", () -> addAdvertRoute)
                ),
                get(() ->
                        path("advert", () -> getAdvertRoute)
                )
        );
    }
}

