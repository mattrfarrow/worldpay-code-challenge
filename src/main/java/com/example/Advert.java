package com.example;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Advert {
    private final float price;
    private final String name;
    private final String description;

    @JsonCreator
    public Advert(@JsonProperty(value="name",required = true) String name,
                  @JsonProperty(value="description", required=true) String description,
                  @JsonProperty(value="price", required=true) float price) {
        this.price = price;
        this.name = name;
        this.description = description;
    }

    public float getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}