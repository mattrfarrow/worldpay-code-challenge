package com.example;

import akka.http.javadsl.model.*;
import akka.http.javadsl.testkit.JUnitRouteTest;
import akka.http.javadsl.testkit.TestRoute;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;


public class AdvertTest extends JUnitRouteTest {
    private TestRoute appRoute = testRoute(new WorldPayAdvertApp().createRoute());

    @Test
    public void successfulAdvertAdd() throws Exception{
        String description = "It's a sofa";
        String name = "Sofa";
        float price = 42f;

        String bodyString = "{ \"name\": \""+name+"\"," +
                " \"price\" : "+price + ", " +
                " \"description\": \""+description+"\" }";

        String entity = appRoute.run(HttpRequest.POST("/addadvert")
                .withEntity( ContentTypes.APPLICATION_JSON, bodyString ))
                .assertStatusCode(StatusCodes.OK)
                .entityString();

        ObjectMapper mapper = new ObjectMapper();

        JsonNode actualObj = mapper.readTree(entity);
        String createdId = actualObj.get("id").asText();

        String getAdvertResponse = appRoute.run(HttpRequest.GET("/advert?id="+createdId))
                .assertStatusCode(StatusCodes.OK)
                .entityString();
        System.out.println("returned advert: " + getAdvertResponse);

        JsonNode response = mapper.readTree(getAdvertResponse);
        Assert.assertEquals(name, response.get("name").textValue());
        Assert.assertEquals(description, response.get("description").textValue());
        Assert.assertEquals(price, response.get("price").floatValue(), 0.01);
    }

    @Test
    public void badRequestOnMissingName() throws Exception{
        String description = "It's a sofa";
        float price = 42f;

        String bodyString = "{ \"price\" : "+price + ", " +
                " \"description\": \""+description+"\" }";

        appRoute.run(HttpRequest.POST("/addadvert")
                .withEntity( ContentTypes.APPLICATION_JSON, bodyString ))
                .assertStatusCode(StatusCodes.BAD_REQUEST);
    }

    @Test
    public void badRequestOnMissingDescription() throws Exception{
        String name = "Sofa";
        float price = 42f;

        String bodyString = "{ \"name\": \""+name+"\"," +
                " \"price\" : "+price + "}";

        appRoute.run(HttpRequest.POST("/addadvert")
                .withEntity( ContentTypes.APPLICATION_JSON, bodyString ))
                .assertStatusCode(StatusCodes.BAD_REQUEST);
    }

    @Test
    public void badRequestOnMissingPrice() throws Exception{
        String description = "It's a sofa";
        String name = "Sofa";

        String bodyString = "{ \"name\": \""+name+"\"," +
                " \"description\": \""+description+"\" }";

        appRoute.run(HttpRequest.POST("/addadvert")
                .withEntity( ContentTypes.APPLICATION_JSON, bodyString ))
                .assertStatusCode(StatusCodes.BAD_REQUEST);
    }
}